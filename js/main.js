
// Flickity
if (typeof Flickity !== "undefined") {
    var utils = window.fizzyUIUtils;
    var carousel = document.querySelector('[data-carousel]');
    var slides = document.getElementsByClassName('carousel-cell');
    var flkty = new Flickity(carousel, {
        prevNextButtons: false,
        wrapAround: true,
        pageDots: false,
        setGallerySize: false,
        bgLazyLoad: true,
        selectedAttraction: 0.035
    });
    // next
    var nextButton = document.querySelectorAll('.button-next');
    for (var i = 0; i < nextButton.length; i++) {
        nextButton[i].addEventListener('click', function () {
            flkty.next();
        });
    }
    
    // previous
    var previousButton = document.querySelectorAll('.button-previous');
    for (var i = 0; i < previousButton.length; i++) {
        previousButton[i].addEventListener('click', function () {
            flkty.previous();
        });
    }
}

// Smooth Scroll
if (typeof SmoothScroll !== "undefined") {
    var scroll = new SmoothScroll('a[href*="#"]:not([data-easing])');
}

// Masonry
if (typeof Masonry !== "undefined") {
    var grid = document.querySelector('.feed__grid');
    var msnry;
    
    imagesLoaded(grid, function () {
        msnry = new Masonry(grid, {
            itemSelector: '.feed__grid-item',
            columnWidth: '.feed__grid-sizer',
            percentPosition: true,
            gutter: 20
        });
    });
}

// Form validation
if (typeof JustValidate !== "undefined") {

    new window.JustValidate('.contact-form', {
        colorWrong: "#fff",
        rules: {
            firstname: {
                required: true
            },
            lastname: {
                required: true
            },
            email: {
                required: true,
                email: true
            },
            message: {
                required: true
            },    
        },
        messages: {
            firstname: {
                required: 'We need your first name.'
            },
            lastname: {
                required: 'We need your last name.'
            },
            email: {
                required: 'Please use a valid e-mail address.'
            },
            message: {
                required: 'Sorry, your message can’t be empty.'
            }
        }
    });
}